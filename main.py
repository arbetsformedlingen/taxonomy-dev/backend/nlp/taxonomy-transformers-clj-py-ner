import torch
from transformers import AutoModel,AutoTokenizer

tokenizer = AutoTokenizer.from_pretrained('KB/bert-base-swedish-cased-ner')

model = AutoModel.from_pretrained('KB/bert-base-swedish-cased-ner')


from transformers import DataCollatorForTokenClassification
from transformers import TrainingArguments

# training_args = TrainingArguments(output_dir="test_trainer")

# import numpy as np

import evaluate

# metric = evaluate.load("accuracy")

# def compute_metrics(eval_pred):

#     logits, labels = eval_pred

#     predictions = np.argmax(logits, axis=-1)

#     return metric.compute(predictions=predictions, references=labels)


from transformers import TrainingArguments, Trainer

# training_args = TrainingArguments(output_dir="test_trainer", evaluation_strategy="epoch")



# trainer = Trainer(
#     model=model,
#     args=training_args,
#     train_dataset=small_train_dataset,
#     eval_dataset=small_eval_dataset,
#     compute_metrics=compute_metrics,
# )

# trainer.train()


class NerDataset(torch.utils.data.Dataset):
    def __init__(self, encodings, labels):
        self.encodings = encodings
        self.labels = labels

    def __getitem__(self, idx):
        item = {key: torch.tensor(val[idx]) for key, val in self.encodings.items()}
        item['labels'] = torch.tensor(self.labels[idx])
        return item

    def __len__(self):
        return len(self.labels)




from clojurebridge import cljbridge

cljbridge.init_jvm(start_repl=True, port=12345,bind='0.0.0.0',mvn_local_repo='/tmp/.m2/repository', aliases=['jdk-17','fastcall'])
