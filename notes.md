(setq python-shell-interpreter "venv/bin/python3")
run-python

///

https://huggingface.co/docs/transformers/glossary#input-ids

For token classification models, (BertForTokenClassification), the model expects a tensor of dimension (batch_size, seq_length) with each value corresponding to the expected label of each individual token.



////

https://msync.org/notes/ml-in-clojure-via-python/

  (require-python '[tokenizers
                    :refer [BertWordPieceTokenizer
                            SentencePieceBPETokenizer
                            CharBPETokenizer
                            ByteLevelBPETokenizer]])
  (require-python '[transformers
                    :refer [BertTokenizer]])

  ;; Files downloaded from
  ;; https://s3.amazonaws.com/models.huggingface.co/bert/gpt2-vocab.json
  ;; https://s3.amazonaws.com/models.huggingface.co/bert/gpt2-merges.txt

  (def tokenizer (ByteLevelBPETokenizer "gpt2-vocab.json" "gpt2-merges.txt"))

  (def encoded (py/a$ tokenizer encode "I can feel the magic, can you?"))

  (py/py.- encoded #_type_ids #_tokens offsets)
