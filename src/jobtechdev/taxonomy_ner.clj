(ns jobtechdev.taxonomy-ner
  (:require
   [libpython-clj2.require :refer [require-python]]
   [libpython-clj2.python :refer [py. py.. py.- py* py**] :as py])
  (:gen-class))

(py/initialize!)

(require-python '[torch])
(require-python '[transformers])


;; (require-python 'torch.utils.data.DataLoader :as DataLoader)

(require-python '[torch.utils.data.DataLoader :as DataLoader])


;; from transformers import DistilBertForTokenClassification, AdamW



;; TODO this one is not working!! ->
(if (py.. torch/cuda is_available)
  (torch/device "cuda:0")
  (torch/device "cpu")
  ;;(torch/device "cpu")
  )


(defn greet
  "Callable entry point to the application."
  [data]
  (println (str "Hello, " (or (:name data) "World") "!")))

(defn -main
  "I don't do a whole lot ... yet."
  [& args]
  (greet {:name (first args)}))


(def tokenizer (py. transformers/AutoTokenizer from_pretrained "KB/bert-base-swedish-cased-ner"))
(def model (py. transformers/AutoModel from_pretrained "KB/bert-base-swedish-cased-ner"))

(def label2id (py.- (py.- model config) "label2id"))
(def id2label (py.- (py.- model config) "id2label"))

(py/set-item! label2id "skill" 14)
(py/set-item! id2label 14 "skill")

(py/set-item! label2id "occupation-name" 15)
(py/set-item! id2label 15 "occupation-name")




(defn tokenize-text [text]
  (tokenizer text))

(defn convert-ids-to-tokens [tokenized-text]
  (py. tokenizer convert_ids_to_tokens (py.- tokenized-text "input_ids")))

(defn char-index-to-token [tokens char-index]
  (py. tokens char_to_token char-index))


(def example-annotation
  {:text "Du kan truck och kontaktmannaskap i Stockholm.",
   :sha1 "14c6a9163deda06d0c925a042894969a987927da",
   :annotations
   [{:concept-id "xLxq_sQ7_RzR",
     :preferred-label "Kontaktmannaskap",
     :type "skill",
     :start-position 17,
     :end-position 33,
     :matched-string "kontaktmannaskap",
     :annotation-id 1,
     :sentiment "MUST_HAVE"}
    {:concept-id "pGts_PKK_c8W",
     :preferred-label "Truckvana",
     :type "skill",
     :start-position 7,
     :end-position 12,
     :matched-string "truck",
     :annotation-id 0,
     :sentiment "MUST_HAVE"}]}
  )

(defn update-label-ids [labels start end class-type]
  (reduce (fn [acc element]
            (assoc acc element class-type)
            )
          labels
          (vec (range start (+ end 1)))
          )
  )

(defn annotations->labels [annotated-doc]
  (let  [tokens (tokenizer (:text annotated-doc))
         tokens-length (count (py.- tokens input_ids))
         label-ids (reduce
                    (fn [label-ids annotation ]
                      (let [{:keys [start-position end-position type]} annotation
                            first-token-index (char-index-to-token tokens start-position)
                            last-token-index (char-index-to-token tokens (- end-position 1))
                            ]

                        (update-label-ids label-ids first-token-index last-token-index
                                          (get (py/->jvm label2id) type 0))
                        ))

                    (vec (repeat tokens-length 0)) (:annotations annotated-doc))
         ]
    #_(py/set-item! tokens "labels" label-ids)
    (py/set-item! tokens "token_type_ids" label-ids)
    ))

(def example-training-data
  (py/->python [(annotations->labels example-annotation)])
  )


(def training-kwargs
  (transformers/TrainingArguments


                                  :output_dir "./results"          ; output directory
                                  :num_train_epochs 3              ; total number of training epochs
                                  :per_device_train_batch_size 2   ; batch size per device during training
                                  :per_device_eval_batch_size 2,   ; batch size for evaluation
                                  :warmup_steps 500,                ; number of warmup steps for learning rate scheduler
                                  :weight_decay 0.01               ; strength of weight decay
                                  :logging_dir './logs'            ; directory for storing logs
                                  :logging_steps 10

                                  ))

(def data-collator (transformers/DataCollatorForTokenClassification  :tokenizer tokenizer))

(defn create-trainer []
  (transformers/Trainer
   :model model
   :args training-kwargs
   :train_dataset example-training-data
   :eval_dataset example-training-data
   :data_collator data-collator
   :tokenizer tokenizer ))

;; TODO kolla id2label och data_collator=data_collator,
(defn train! []
  (py. (create-trainer) train))


(defn create-ner-dataset [encodings, labels]
  (let [main-mod (py/add-module "__main__")
        mod-dict (py/module-dict main-mod)
        ner-dataset-class (get mod-dict "NerDataset")
        ]
    (ner-dataset-class encodings labels)
    ))


#_(defn create-class-ner-dataset []
  (py/run-simple-string "class NerDataset(torch.utils.data.Dataset):
    def __init__(self, encodings, labels):
        self.encodings = encodings
        self.labels = labels

    def __getitem__(self, idx):
        item = {key: torch.tensor(val[idx]) for key, val in self.encodings.items()}
        item['labels'] = torch.tensor(self.labels[idx])
        return item

    def __len__(self):
        return len(self.labels)
")
  )



#_(def NER-Dataset (py/create-class
              "NerDataset"
              'torch.utils.data.Dataset
              {"__init__" (py/make-instance-fn
                           (fn [this encodings, labels]
                             (py/set-attr! this "encodings" encodings)
                             (py/set-attr! this "labels" labels)
                             ;;If you don't return nil from __init__ that is an
                             ;;error.
                             nil))
               "__getitem__" (py/make-instance-fn   ;; NOT IMPLEMENTED
                              (fn [this idx]

                                (for [item (py/get-attr e "items")]

                                  )
                                (+ (py/get-attr this "arg")
                                   otherarg)))
               "__len__" (py/make-instance-fn
                         (fn [this]
                           (count (py/get-attr this "labels"))))
               "addarg" (py/make-instance-fn
                         (fn [this otherarg]
                           (+ (py/get-attr this "arg")
                              otherarg)))
               }))



;; #_{

;;    TODO:

;;    Plocka ut label ids från modellen

;;    Tokenisera texten -> lista med tokens

;;    Skapa en lista som är like lång som listan med tokens med rätt NER label id utifrån annoterade datat

;;    Mutera den tokeniserade texten, lägg till ett attribut "labels" med NER label ids

;;    tokenized_inputs["labels"] = labels

;;     https://huggingface.co/docs/transformers/tasks/token_classification


;;    tokenized_wnut = wnut.map(tokenize_and_align_labels, batched=True)

;;    använd den muterade tokeniserade texten till din Trainer

;;    }

;; #_{
;; training_args = TrainingArguments(
;;                                   output_dir="my_awesome_wnut_model",
;;                                   learning_rate=2e-5,
;;                                   per_device_train_batch_size=16,
;;                                   per_device_eval_batch_size=16,
;;                                   num_train_epochs=2,
;;                                   weight_decay=0.01,
;;                                   evaluation_strategy="epoch",
;;                                   save_strategy="epoch",
;;                                   load_best_model_at_end=True,
;;                                   push_to_hub=True,
;;                                   )

;; trainer = Trainer(
;;                   model=model,
;;                   args=training_args,
;;                   train_dataset=tokenized_wnut["train"],
;;                   eval_dataset=tokenized_wnut["test"],
;;                   tokenizer=tokenizer,
;;                   data_collator=data_collator,
;;                   compute_metrics=compute_metrics,
;;                   )

;;    trainer.train()
;;  }
