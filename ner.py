from pathlib import Path
import re


import os, json
import pandas as pd


from transformers import BertTokenizerFast
from transformers import AutoModel,AutoTokenizer

#model = AutoModel.from_pretrained('KB/bert-base-swedish-cased-ner')

tokenizer = BertTokenizerFast.from_pretrained('KB/bert-base-swedish-cased-ner')

#train_encodings = tokenizer(train_texts, is_split_into_words=True, return_offsets_mapping=True, padding=True, truncation=True)



from transformers import BertForTokenClassification
model = BertForTokenClassification.from_pretrained('KB/bert-base-swedish-cased-ner')



label2id =  model.config.label2id
id2label =  model.config.id2label

label2id["occupation-name"] = 14
label2id["skill"] = 15

id2label[14] = "occupation-name"
id2label[15] = "skill"


unique_tags = len(label2id.keys())

path_to_json = '../mentor-api-prod'
json_files = [pos_json for pos_json in os.listdir(path_to_json) if pos_json.endswith('.json')]
# print(json_files)  # for me this prints ['foo.json']


# annotated_doc_1 = pd.DataFrame.from_dict(json_files[0])

def read_annotated_docs(fs, idx):
    file_name = '../mentor-api-prod/' + fs[idx]
    file = open(file_name)
    doc = json.load(file)
    return doc


def doc_to_encoding(doc):
    encoding = tokenizer(doc["text"], padding="max_length", truncation=True, max_length = 512 )

    # encoded_labels = [0] * len(encoding["input_ids"])

    for a in doc['annotations']:
        start_token_i = encoding.char_to_token(a['start-position'])
        end_token_i = encoding.char_to_token(a['end-position'] - 1)
        taxonomy_type = a.get("type")
        if start_token_i != None and end_token_i != None :
            for i in range(start_token_i, end_token_i):
                encoding["token_type_ids"][i] = label2id.get(taxonomy_type, 0)

    return encoding


def read_wnut(file_path):
    file_path = Path(file_path)

    raw_text = file_path.read_text().strip()
    raw_docs = re.split(r'\n\t?\n', raw_text)
    token_docs = []
    tag_docs = []
    for doc in raw_docs:
        tokens = []
        tags = []
        for line in doc.split('\n'):
            token, tag = line.split('\t')
            tokens.append(token)
            tags.append(tag)
        token_docs.append(tokens)
        tag_docs.append(tags)

    return token_docs, tag_docs

# texts, tags = read_wnut('wnut17train.conll')


# from sklearn.model_selection import train_test_split
# train_texts, val_texts, train_tags, val_tags = train_test_split(texts, tags, test_size=.2)



# unique_tags = set(tag for doc in tags for tag in doc)
# tag2id = {tag: id for id, tag in enumerate(unique_tags)}
# id2tag = {id: tag for tag, id in tag2id.items()}



# from transformers import DistilBertTokenizerFast
# tokenizer = DistilBertTokenizerFast.from_pretrained('distilbert-base-cased')

# train_encodings = tokenizer(train_texts[0:1], is_split_into_words=True, return_offsets_mapping=True, padding=True, truncation=True)
# val_encodings = tokenizer(val_texts, is_split_into_words=True, return_offsets_mapping=True, padding=True, truncation=True)



import numpy as np

# def encode_tags(tags, encodings):
#     labels = [[tag2id[tag] for tag in doc] for doc in tags]
#     encoded_labels = []
#     for doc_labels, doc_offset in zip(labels, encodings.offset_mapping):
#         # create an empty array of -100
#         doc_enc_labels = np.ones(len(doc_offset),dtype=int) * -100
#         arr_offset = np.array(doc_offset)

#         # set labels whose first offset position is 0 and the second is not 0
#         doc_enc_labels[(arr_offset[:,0] == 0) & (arr_offset[:,1] != 0)] = doc_labels
#         encoded_labels.append(doc_enc_labels.tolist())

#     return encoded_labels

# train_labels = encode_tags(train_tags, train_encodings)
# val_labels = encode_tags(val_tags, val_encodings)


import torch

# Skapa en  dict
# som har keys "input_ids"
# token_type_ids
# som är en torch.tensor


# class WNUTDataset(torch.utils.data.Dataset):
#     def __init__(self, encodings, labels):
#         self.encodings = encodings
#         self.labels = labels

#     def __getitem__(self, idx):
#         item = {key: torch.tensor(val[idx]) for key, val in self.encodings.items()}
#         item['labels'] = torch.tensor(self.labels[idx])
#         return item

#     def __len__(self):
#         return len(self.labels)


class Annotations_Dataset(torch.utils.data.Dataset):
    # Characterizes a dataset for PyTorch
    def __init__(self, annotated_doc_filenames):
        self.annotated_doc_filenames = annotated_doc_filenames

    def __len__(self):
        return len(self.annotated_doc_filenames)

    def get_item(self, index):
        return self.__getitem__(index)

    def __getitem__(self, index):
        doc = read_annotated_docs(self.annotated_doc_filenames, index)
        encoding = doc_to_encoding(doc)
        encoding["input_ids"] = torch.tensor(encoding["input_ids"])
        encoding["token_type_ids"] = torch.tensor(encoding["token_type_ids"])
        encoding["attention_mask"] = torch.tensor(encoding["attention_mask"])
        return encoding

# train_encodings.pop("offset_mapping") # we don't want to pass this to the model
# val_encodings.pop("offset_mapping")
# train_dataset = WNUTDataset(train_encodings, train_labels)
# val_dataset = WNUTDataset(val_encodings, val_labels)



# train_labels = encode_tags(train_tags, train_encodings)
# val_labels = encode_tags(val_tags, val_encodings)


# train_dataset = WNUTDataset(train_encodings, train_labels)
# val_dataset = WNUTDataset(val_encodings, val_labels)



# from transformers import DistilBertForTokenClassification
# model = DistilBertForTokenClassification.from_pretrained('distilbert-base-cased', num_labels=len(unique_tags))



# from transformers import DistilBertForSequenceClassification, Trainer, TrainingArguments

# training_args = TrainingArguments(
#     output_dir='./results',          # output directory
#     num_train_epochs=3,              # total number of training epochs
#     per_device_train_batch_size=16,  # batch size per device during training
#     per_device_eval_batch_size=64,   # batch size for evaluation
#     warmup_steps=500,                # number of warmup steps for learning rate scheduler
#     weight_decay=0.01,               # strength of weight decay
#     logging_dir='./logs',            # directory for storing logs
#     logging_steps=10,
# )

# model = DistilBertForTokenClassification.from_pretrained("distilbert-base-uncased")

# trainer = Trainer(
#     model=model,                         # the instantiated 🤗 Transformers model to be trained
#     args=training_args,                  # training arguments, defined above
#     train_dataset=train_dataset,         # training dataset
#     eval_dataset=val_dataset             # evaluation dataset
# )

# trainer.train()






from torch.utils.data import DataLoader
from transformers import DistilBertForTokenClassification, AdamW

device = torch.device('cuda') if torch.cuda.is_available() else torch.device('cpu')

model = BertForTokenClassification.from_pretrained('KB/bert-base-swedish-cased-ner' , num_labels=unique_tags, ignore_mismatched_sizes=True, id2label = id2label, label2id = label2id)

model.to(device)
model.train()

# train_loader = DataLoader(train_dataset, batch_size=16, shuffle=True)

train_dataset =  Annotations_Dataset(json_files)
train_loader = DataLoader(train_dataset, batch_size=8, shuffle=True)

optim = AdamW(model.parameters(), lr=5e-5)

for epoch in range(3):
    for batch in train_loader:
        optim.zero_grad()
        input_ids = batch['input_ids'].to(device)
        attention_mask = batch['attention_mask'].to(device)
        labels = batch['token_type_ids'].to(device)
        outputs = model(input_ids, attention_mask=attention_mask, labels=labels)
        loss = outputs[0]
        loss.backward()
        optim.step()

model.eval()

model.save_pretrained("my_model2/")

# RuntimeError: stack expects each tensor to be equal size, but got [917] at entry 0 and [479] at entry 1
