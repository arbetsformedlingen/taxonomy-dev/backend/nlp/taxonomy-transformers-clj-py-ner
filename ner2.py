import torch

from transformers import BertForTokenClassification
from transformers import BertTokenizerFast

from transformers import pipeline
model = BertForTokenClassification.from_pretrained('/home/qeshi/repo/clojure/taxonomy-ner/my_model2')


# classifier = pipeline("ner", model='/home/qeshi/repo/clojure/taxonomy-ner/my_model')

from transformers import AutoTokenizer



tokenizer = BertTokenizerFast.from_pretrained('KB/bert-base-swedish-cased-ner')


text = "Du kan java och ska jobba som en utvecklare."

inputs = tokenizer(text, return_tensors="pt")


with torch.no_grad():
    logits = model(**inputs).logits


predictions = torch.argmax(logits, dim=2)

predicted_token_class = [model.config.id2label[t.item()] for t in predictions[0]]
